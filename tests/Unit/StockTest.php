<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\Api\OrderController;

class StockTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();        
        $this->seed();
        $this->withoutExceptionHandling();        
    }

    /**
     * A unit test to verify stock
     *
     * @return void
     */
    public function test_verify_stock()
    {     
        $orderObject = new OrderController();
        $productId = 1;
        $quantity  = 5;
        $orderObject->verifyStock($productId,$quantity);
        $this->assertTrue(TRUE);        
    }

     /**
     * A unit test to update stock.
     *
     * @return void
     */
    public function test_update_stock()
    {     
        $orderObject = new OrderController();        
        $requestData = [
            "1" => 2,
            "2" => 3
        ];
        $orderObject->updateStockSendEmail($requestData);
        $this->assertTrue(TRUE);        
    }
}
