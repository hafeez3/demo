<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = Product::create([
            'name' => 'small burger',
            'type' => 'burger'
        ]);
        $product->ingredients()->attach([
            1 => ['quantity'=>150],
            2 => ['quantity'=>30],
            3 => ['quantity'=>20]
        ]);
        $product = Product::create([
            'name' => 'small pizza',
            'type' => 'pizza'
        ]);
        $product->ingredients()->attach([
            2 => ['quantity'=>30],
            3 => ['quantity'=>20]
        ]);
        $product = Product::create([
            'name' => 'large burger',
            'type' => 'burger'
        ]);
        $product->ingredients()->attach([
            1 => ['quantity'=>200],
            2 => ['quantity'=>50],
            3 => ['quantity'=>40]
        ]);
    }
}
